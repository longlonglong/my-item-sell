import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'

// 引入全局样式
import './assets/css/reset.less'


Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')